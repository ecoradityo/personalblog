<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property integer $bank_id
 * @property string $bank_name
 * @property integer $interest
 * @property integer $max_tenor
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'interest', 'max_tenor'], 'required'],
            [['interest', 'max_tenor'], 'integer'],
            [['bank_name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bank_id' => 'Bank ID',
            'bank_name' => 'Bank Name',
            'interest' => 'Interest',
            'max_tenor' => 'Max Tenor',
        ];
    }

    public function getPengajuan()
    {
        return $this->hasMany(Pengajuan::className(), ['bank_id' => 'bank_id']);
    }
}
