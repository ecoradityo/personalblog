<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengajuan".
 *
 * @property integer $pengajuan_id
 * @property integer $customer_id
 * @property integer $vehicle_value
 * @property integer $vehicle_status
 * @property string $pengajuan_date
 * @property integer $pengajuan_status
 * @property integer $down_payment
 * @property integer $tenor
 * @property integer $bank_id
 */
class Pengajuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengajuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'vehicle_value', 'vehicle_status', 'pengajuan_date', 'pengajuan_status', 'down_payment', 'tenor', 'bank_id'], 'required'],
            [['customer_id', 'vehicle_value', 'vehicle_status', 'pengajuan_status', 'down_payment', 'tenor', 'bank_id'], 'integer'],
            [['pengajuan_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pengajuan_id' => 'Pengajuan ID',
            'customer_id' => 'Customer ID',
            'vehicle_value' => 'Vehicle Value',
            'vehicle_status' => 'Vehicle Status',
            'pengajuan_date' => 'Pengajuan Date',
            'pengajuan_status' => 'Pengajuan Status',
            'down_payment' => 'Down Payment',
            'tenor' => 'Tenor',
            'bank_id' => 'Bank ID',
        ];
    }

    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['bank_id' => 'bank_id']);
    }
}
