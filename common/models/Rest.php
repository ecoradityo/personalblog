<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rest".
 *
 * @property string $Name
 * @property string $City
 * @property string $Country
 */
class Rest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'City', 'Country'], 'required'],
            [['Name', 'City', 'Country'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Name' => 'Name',
            'City' => 'City',
            'Country' => 'Country',
        ];
    }
}
