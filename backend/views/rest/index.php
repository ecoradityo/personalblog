<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rest-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Rest', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Name',
            'City',
            'Country',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
