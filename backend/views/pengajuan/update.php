<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pengajuan */

$this->title = 'Update Pengajuan: ' . $model->pengajuan_id;
$this->params['breadcrumbs'][] = ['label' => 'Pengajuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pengajuan_id, 'url' => ['view', 'id' => $model->pengajuan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengajuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
