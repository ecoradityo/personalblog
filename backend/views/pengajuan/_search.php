<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PengajuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengajuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pengajuan_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'vehicle_value') ?>

    <?= $form->field($model, 'vehicle_status') ?>

    <?= $form->field($model, 'pengajuan_date') ?>

    <?php // echo $form->field($model, 'pengajuan_status') ?>

    <?php // echo $form->field($model, 'down_payment') ?>

    <?php // echo $form->field($model, 'tenor') ?>

    <?php // echo $form->field($model, 'bank_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
