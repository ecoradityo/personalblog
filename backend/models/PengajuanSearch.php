<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pengajuan;

/**
 * PengajuanSearch represents the model behind the search form of `common\models\Pengajuan`.
 */
class PengajuanSearch extends Pengajuan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengajuan_id', 'customer_id', 'vehicle_value', 'vehicle_status', 'pengajuan_status', 'down_payment', 'tenor', 'bank_id'], 'integer'],
            [['pengajuan_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengajuan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pengajuan_id' => $this->pengajuan_id,
            'customer_id' => $this->customer_id,
            'vehicle_value' => $this->vehicle_value,
            'vehicle_status' => $this->vehicle_status,
            'pengajuan_date' => $this->pengajuan_date,
            'pengajuan_status' => $this->pengajuan_status,
            'down_payment' => $this->down_payment,
            'tenor' => $this->tenor,
            'bank_id' => $this->bank_id,
        ]);

        return $dataProvider;
    }
}
