<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rest;

/**
 * RestSearch represents the model behind the search form of `common\models\Rest`.
 */
class RestSearch extends Rest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'City', 'Country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'Country', $this->Country]);

        return $dataProvider;
    }
}
