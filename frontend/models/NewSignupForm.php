<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Customer;

/**
 * Signup form
 */
class NewSignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $customer_name;
    public $customer_city;
    public $customer_country;
    public $customer_income;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_name', 'customer_city', 'customer_country', 'customer_income'], 'required'],
            ['customer_name', 'string', 'max' => 64],
            [['customer_city', 'customer_country'], 'string', 'max' =>32],
            ['customer_income', 'integer'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new Customer();
        $user->customer_name = $this->customer_name;
        $user->customer_city = $this->customer_city;
        $user->customer_country = $this->customer_country;
        $user->customer_income = $this->customer_income;

        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
