<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PengajuanForm extends Model
{
    public $nilaikendaraan;
    public $statuskendaraan;
    public $uangmuka;
    public $tenor;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nilaikendaraan', 'statuskendaraan', 'uangmuka', 'tenor'], 'required'],
            [['nilaikendaraan', 'statuskendaraan', 'uangmuka', 'tenor'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nilaikendaraan' => 'Nilai Kendaraan',
            'statuskendaraan' => 'Status Kendaraan',
            'uangmuka' => 'Uang Muka',
            'tenor' => 'Tenor',
        ];
    }
}
