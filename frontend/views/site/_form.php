<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Bank;

/* @var $this yii\web\View */
/* @var $model common\models\Pengajuan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengajuan-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'customer_id')->textInput() ?> -->

    <?= $form->field($model, 'vehicle_value')->textInput() ?>

    <?php
        echo $form->field($model, 'vehicle_status')->dropDownList(['0' => 'Used', '10' => 'New'],['prompt' => 'Vehicle Status'])
    ?>

    <?= $form->field($model, 'pengajuan_date')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    //'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'down_payment')->textInput() ?>

    <?= $form->field($model, 'tenor')->textInput() ?>

    <?= Html::activeLabel($model, 'bank_id'); ?>
    <?= Html::activeDropDownList($model, 'bank_id',
      ArrayHelper::map(Bank::find()->all(), 'bank_id', 'bank_name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
